### Requirements 

- [JDK 15](!https://www.oracle.com/java/technologies/javase/jdk15-archive-downloads.html)

### List of Arguments

- `--app.entity` defines what entity we will work with
- `--app.action` defines what action we will perform
- `--app.firstName` sets first name
- `--app.lastName` sets last name
- `--app.gpa` sets gpa
- `--app.name` sets name 
- `--app.lang` sets language
- `--app.studentIds` sets student ids. 
    - use `,` as delimiter
    - for example: `--app.studentIds=1,2,3`
- `--app.teacherId` sets teacher id
- `--app.studentId` sets student id
- `--app.subjectId` sets subject id

### List of supported entities
- `student` targets student entity
- `teacher` targets teacher entity
- `subject` targets subject entity

### List of allowed actions
- `create` creates new entry for given entity
    - for `student` entity you need to set `--app.firstName`, `--app.lastName` and `--app.gpa`
    - for `teacher` entity you need to set `--app.firstName` and `--app.lastName`
    - for `subject` entity you need to set `--app.name`, `--app.lang`, `--app.teacherId` and `--app.studentIds`
- `update` updates row with given id
    - for `student` entity you need to set `--app.studentId`, `--app.firstName`, `--app.lastName` and `--app.gpa`
    - for `teacher` entity you need to set `--app.teacherId`, `--app.firstName` and `--app.lastName`
    - for `subject` entity you need to set `--app.subjectId`, `--app.name`, `--app.lang`, `--app.teacherId` and `--app.studentIds`
- `readAll` returns content from given entity
- `readById` returns row with given id
    - for `student` entity you need to set `--app.studentId`
    - for `teacher` entity you need to set `--app.teacherId`
    - for `subject` entity you need to set `--app.subjectId`
- `readAllByFirstName` returns row with given first name
- `readAllByLastName` returns row with given last name
- `readAllByFullName` returns row with given first and last name
- `readByGpa` return rows with given gpa
- `readByGpaLessThan` return rows that are less than given gpa
- `readByGpaGreaterThan` return rows that are greater than given gpa
- `deleteById` deletes row with given id
    - for `student` entity you need to set `--app.studentId`
    - for `teacher` entity you need to set `--app.teacherId`
    - for `subject` entity you need to set `--app.subjectId`
- `deleteWithNoSubject` deletes teachers which don't have subject


