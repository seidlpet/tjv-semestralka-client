package cz.cvut.fit.tjv.seidlpet.semestralkaclient.resource;

import cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto.StudentCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto.StudentDTO;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component
public class StudentResource {
    private final RestTemplate restTemplate;

    public StudentResource(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private static final String ROOT_URL = "http://localhost:8080/api/v1/student";
    private static final String ONE_URI = "/{id}";
    private static final String ONE_GPA_URI = "/gpa/{gpa}";
    private static final String FIRST_NAME_URI = "?firstName={firstname}";
    private static final String LAST_NAME_URI = "?lastName={lastName}";
    private static final String FULL_NAME_URI = "?firstName={firstName}&lastName={lastName}";
    private static final String LESS_GPA_URI = "/gpa/less?gpa={gpa}";
    private static final String GREATER_GPA_URI = "/gpa/greater?gpa={gpa}";

    public URI create(StudentCreateDTO data) {
        return restTemplate.postForLocation(ROOT_URL,data);
    }

    public List<StudentDTO> readAll() {
        return Arrays.asList(
                Objects.requireNonNull(
                        restTemplate.getForEntity(ROOT_URL,StudentDTO[].class).getBody()
                )
        );
    }

    public StudentDTO readById(int id) {
        return restTemplate.getForObject(ROOT_URL+ONE_URI,StudentDTO.class,id);
    }

    public List<StudentDTO> readByGpa(double gpa) {
        return Arrays.asList(
                Objects.requireNonNull(
                    restTemplate.getForEntity(ROOT_URL+ONE_GPA_URI,StudentDTO[].class,gpa).getBody()
                )
        );
    }

    public List<StudentDTO> readAllByFirstName(String firstName) {
        return Arrays.asList(
                Objects.requireNonNull(
                        restTemplate.getForEntity(ROOT_URL+FIRST_NAME_URI,StudentDTO[].class,firstName).getBody()
                )
        );
    }

    public List<StudentDTO> readAllByLastName(String lastName) {
        return Arrays.asList(
                Objects.requireNonNull(
                        restTemplate.getForEntity(ROOT_URL+LAST_NAME_URI,StudentDTO[].class,lastName).getBody()
                )
        );
    }

    public List<StudentDTO> readAllByFirstAndLastName(String firstName, String lastName) {
        return Arrays.asList(
                Objects.requireNonNull(
                        restTemplate.getForEntity(ROOT_URL+FULL_NAME_URI,StudentDTO[].class,firstName,lastName).getBody()
                )
        );
    }

    public List<StudentDTO> readByGpaGreaterThan(double gpa) {
        return Arrays.asList(
                Objects.requireNonNull(
                        restTemplate.getForEntity(ROOT_URL+GREATER_GPA_URI,StudentDTO[].class,gpa).getBody()
                )
        );
    }

    public List<StudentDTO> readByGpaLesserThan(double gpa) {
        return Arrays.asList(
                Objects.requireNonNull(
                        restTemplate.getForEntity(ROOT_URL+LESS_GPA_URI,StudentDTO[].class,gpa).getBody()
                )
        );
    }

    public void update(int id, StudentCreateDTO data) {
        restTemplate.put(ROOT_URL+ONE_URI,data,id);
    }

    public void deleteById(int id) {
        restTemplate.delete(ROOT_URL+ONE_URI,id);
    }

}
