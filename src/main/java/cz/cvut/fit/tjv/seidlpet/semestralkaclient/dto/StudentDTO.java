package cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto;

public class StudentDTO {
    private final int id;
    private final String firstName;
    private final String lastName;
    private final double gpa;

    public StudentDTO(int id, String firstName, String lastName, double gpa) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gpa = gpa;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getGpa() {
        return gpa;
    }
}
