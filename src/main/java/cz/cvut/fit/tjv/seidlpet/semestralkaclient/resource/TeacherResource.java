package cz.cvut.fit.tjv.seidlpet.semestralkaclient.resource;

import cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto.TeacherDTO;
import cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto.TeacherCreateDTO;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component
public class TeacherResource {
    private final RestTemplate restTemplate;

    public TeacherResource(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private static final String ROOT_URL = "http://localhost:8080/api/v1/teacher";
    private static final String ONE_URI = "/{id}";
    private static final String FIRST_NAME_URI = "?firstName={firstname}";
    private static final String LAST_NAME_URI = "?lastName={lastName}";
    private static final String FULL_NAME_URI = "?firstName={firstName}&lastName={lastName}";
    private static final String NO_SUBJECT_URI = "/no-subject";
    public URI create(TeacherCreateDTO data) {
        return restTemplate.postForLocation(ROOT_URL,data);
    }

    public List<TeacherDTO> readAll() {
        return Arrays.asList(
            Objects.requireNonNull(
                    restTemplate.getForEntity(ROOT_URL, TeacherDTO[].class).getBody()
            )
        );
    }

    public TeacherDTO readById(int id) {
        return restTemplate.getForObject(ROOT_URL+ONE_URI, TeacherDTO.class,id);
    }

    public List<TeacherDTO> readAllByFirstName(String firstName) {
        return Arrays.asList(
                Objects.requireNonNull(
                        restTemplate.getForEntity(ROOT_URL+FIRST_NAME_URI,TeacherDTO[].class,firstName).getBody()
                )
        );
    }

    public List<TeacherDTO> readAllByLastName(String lastName) {
        return Arrays.asList(
                Objects.requireNonNull(
                        restTemplate.getForEntity(ROOT_URL+LAST_NAME_URI,TeacherDTO[].class,lastName).getBody()
                )
        );
    }

    public List<TeacherDTO> readAllByFirstAndLastName(String firstName, String lastName) {
        return Arrays.asList(
                Objects.requireNonNull(
                        restTemplate.getForEntity(ROOT_URL+FULL_NAME_URI,TeacherDTO[].class,firstName,lastName).getBody()
                )
        );
    }
    
    public void update(int id, TeacherCreateDTO data) { restTemplate.put(ROOT_URL+ONE_URI,data,id); }
    
    public void deleteById(int id) { restTemplate.delete(ROOT_URL+ONE_URI,id); }

    public void deleteWithNoSubject() { restTemplate.delete(ROOT_URL+NO_SUBJECT_URI); }
}
