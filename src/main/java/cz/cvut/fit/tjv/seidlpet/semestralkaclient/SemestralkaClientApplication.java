package cz.cvut.fit.tjv.seidlpet.semestralkaclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto.StudentCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto.SubjectCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto.TeacherCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralkaclient.resource.StudentResource;
import cz.cvut.fit.tjv.seidlpet.semestralkaclient.resource.SubjectResource;
import cz.cvut.fit.tjv.seidlpet.semestralkaclient.resource.TeacherResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.HypermediaRestTemplateConfigurer;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class SemestralkaClientApplication implements ApplicationRunner {

    @Autowired
    private SubjectResource subjectResource;

    @Autowired
    private StudentResource studentResource;

    @Autowired
    private TeacherResource teacherResource;

    private final static String INFO = "[INFO] Request returned..\n";
    private final static String NOT_FOUND = "[ERROR] Request returned status code 404..";
    private final static String BAD_REQUEST = "[ERROR] Request returned status code 400..";
    private final static String ERROR_PARAMETER = "[ERROR] Please make sure that u set these optional parameters: ";

    public static void main(String[] args) {
        SpringApplication.run(SemestralkaClientApplication.class, args);
    }

    @Bean
    RestTemplateCustomizer customizer(HypermediaRestTemplateConfigurer c) {
        return c::registerHypermediaTypes;
    }

    @Bean
    RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    /*
        Arguments:
            • app.entity - select entity
            • app.action - select action
            • app.firstName
            • app.lastName
            • app.teacherId
            • app.gpa
     */
    @Override
    public void run(ApplicationArguments args) {
        if(args.getOptionValues("app.action") == null || args.getOptionValues("app.entity") == null) {
            System.out.println(ERROR_PARAMETER+"--app.entity and --app.action");
            return;
        }
        String action = args.getOptionValues("app.action").get(0);
        String entity = args.getOptionValues("app.entity").get(0);
        String lang = (args.getOptionValues("app.lang") != null ? args.getOptionValues("app.lang").get(0) : null);
        String name = (args.getOptionValues("app.name") != null ? args.getOptionValues("app.name").get(0) : null);
        String studentIds = (args.getOptionValues("app.studentIds") != null ? args.getOptionValues("app.studentIds").get(0) : null);
        String firstName = (args.getOptionValues("app.firstName") != null ? args.getOptionValues("app.firstName").get(0) : null);
        String lastName = (args.getOptionValues("app.lastName") != null ? args.getOptionValues("app.lastName").get(0) : null);
        double gpa = (args.getOptionValues("app.gpa") != null ? Double.parseDouble(args.getOptionValues("app.gpa").get(0)) : -1);
        int teacherId = (args.getOptionValues("app.teacherId") != null ? Integer.parseInt(args.getOptionValues("app.teacherId").get(0)) : -1);
        int studentId = (args.getOptionValues("app.studentId") != null ? Integer.parseInt(args.getOptionValues("app.studentId").get(0)) : -1);
        int subjectId = (args.getOptionValues("app.subjectId") != null ? Integer.parseInt(args.getOptionValues("app.subjectId").get(0)) : -1);

        List<String> allowedEntities = Arrays.asList("student","teacher","subject");
        List<String> allowedActionsStudent = Arrays.asList("create","update","deleteById",
                "readAll","readById","readAllByFirstName","readAllByLastName","readAllByFullName",
                "readByGpa","readByGpaLessThan","readByGpaGreaterThan");
        List<String> allowedActionsSubject = Arrays.asList("create","update","deleteById",
                "readAll","readById","readAllByTeacherId");
        List<String> allowedActionsTeacher = Arrays.asList("create","update","deleteById","deleteWithNoSubject",
                "readAll","readById","readAllByFirstName","readAllByLastName","readAllByFullName");

        if(!allowedEntities.contains(entity)) {
            System.out.println("[ERROR] Please select one of the supported actions:\n"+Arrays.toString(allowedEntities.toArray()));
            return;
        }

        if(entity.equals("student") && !allowedActionsStudent.contains(action)) {
            System.out.println("[ERROR] Please select one of the supported actions for "+entity+" entity:\n"+Arrays.toString(allowedActionsStudent.toArray()));
            return;
        } else if(entity.equals("teacher") && !allowedActionsTeacher.contains(action)) {
            System.out.println("[ERROR] Please select one of the supported actions for "+entity+" entity:\n"+Arrays.toString(allowedActionsTeacher.toArray()));
            return;
        } else if(entity.equals("subject") && !allowedActionsSubject.contains(action)){
            System.out.println("[ERROR] Please select one of the supported actions for "+entity+" entity:\n"+Arrays.toString(allowedActionsSubject.toArray()));
            return;
        }

        switch (action) {
            case "create":
                try {
                    if(entity.equals("student")) {
                        if(firstName == null || lastName == null || gpa == -1) {
                            System.out.println(ERROR_PARAMETER+"--app.firstName, --app.lastName and --app.gpa");
                            return;
                        }
                        studentResource.create(new StudentCreateDTO(firstName, lastName, gpa));
                    } else if(entity.equals("teacher")) {
                        if(firstName==null || lastName == null) {
                            System.out.println(ERROR_PARAMETER+"--app.firstName and --app.lastName");
                            return;
                        }
                        teacherResource.create(new TeacherCreateDTO(firstName,lastName));
                    } else {
                        if(name == null || lang == null || teacherId == -1 || studentIds == null) {
                            System.out.println(ERROR_PARAMETER+"--app.name, --app.lang, --app.teacherId and --app.studentIds");
                            return;
                        }
                        subjectResource.create(new SubjectCreateDTO(name,lang,teacherId,Arrays.stream(studentIds.split(",")).map(Integer::parseInt).collect(Collectors.toList())));
                    }
                } catch (HttpStatusCodeException e) {
                    System.out.println(BAD_REQUEST);
                }
                break;

            case "readAll":
                if(entity.equals("student"))
                    System.out.println(INFO+asJsonString(studentResource.readAll()));
                else if(entity.equals("teacher"))
                    System.out.println(INFO+asJsonString(teacherResource.readAll()));
                else
                    System.out.println(INFO+asJsonString(subjectResource.readAll()));
                break;

            case "readById":
                try {
                    if(entity.equals("student")) {
                        if (studentId == -1) {
                            System.out.println(ERROR_PARAMETER + "--app.studentId");
                            return;
                        }
                        System.out.println(INFO + asJsonString(studentResource.readById(studentId)));
                    } else if(entity.equals("teacher")) {
                        if (teacherId == -1) {
                            System.out.println(ERROR_PARAMETER+"--app.teacherId");
                            return;
                        }
                        System.out.println(INFO+asJsonString(teacherResource.readById(teacherId)));
                    } else {
                        if (subjectId == -1) {
                            System.out.println(ERROR_PARAMETER+"--app.subjectId");
                            return;
                        }
                        System.out.println(INFO+asJsonString(subjectResource.readById(subjectId)));
                    }
                } catch (HttpStatusCodeException e) {
                    System.out.println(NOT_FOUND);
                }
                break;

            case "readAllByFirstName":
                try {
                    if (firstName == null) {
                        System.out.println(ERROR_PARAMETER + "--app.firstName");
                        return;
                    }
                    if(entity.equals("student"))
                        System.out.println(INFO + asJsonString(studentResource.readAllByFirstName(firstName)));
                    else
                        System.out.println(INFO + asJsonString(teacherResource.readAllByFirstName(firstName)));
                } catch (HttpStatusCodeException e) {
                    System.out.println(NOT_FOUND);
                }
                break;

            case "readAllByLastName":
                try {
                    if(lastName == null) {
                        System.out.println(ERROR_PARAMETER+"--app.lastName");
                        return;
                    }
                    if(entity.equals("student"))
                        System.out.println(INFO+asJsonString(studentResource.readAllByLastName(lastName)));
                    else
                        System.out.println(INFO+asJsonString(teacherResource.readAllByLastName(lastName)));
                } catch (HttpStatusCodeException e) {
                    System.out.println(NOT_FOUND);
                }
                break;

            case "readAllByFullName":
                try {
                    if(firstName == null || lastName == null) {
                        System.out.println(ERROR_PARAMETER+"--app.firstName and --app.lastName");
                        return;
                    }
                    if(entity.equals("student"))
                        System.out.println(INFO+asJsonString(studentResource.readAllByFirstAndLastName(firstName,lastName)));
                    else
                        System.out.println(INFO+asJsonString(teacherResource.readAllByFirstAndLastName(firstName,lastName)));
                } catch (HttpStatusCodeException e) {
                    System.out.println(NOT_FOUND);
                }
                break;

            case "readByGpa":
                try {
                    if(gpa == -1) {
                        System.out.println(ERROR_PARAMETER+"--app.gpa");
                        return;
                    }
                    System.out.println(INFO+asJsonString(studentResource.readByGpa(gpa)));
                } catch (HttpStatusCodeException e) {
                    System.out.println(NOT_FOUND);
                }
                break;

            case "readByGpaLessThan":
                try {
                    if(gpa == -1) {
                        System.out.println(ERROR_PARAMETER+"--app.gpa");
                        return;
                    }
                    System.out.println(INFO+asJsonString(studentResource.readByGpaLesserThan(gpa)));
                } catch (HttpStatusCodeException e) {
                    System.out.println(NOT_FOUND);
                }
                break;

            case "readByGpaGreaterThan":
                try {
                    if(gpa == -1) {
                        System.out.println(ERROR_PARAMETER+"--app.gpa");
                        return;
                    }
                    System.out.println(INFO+asJsonString(studentResource.readByGpaGreaterThan(gpa)));
                } catch (HttpStatusCodeException e) {
                    System.out.println(NOT_FOUND);
                }
                break;
            case "readAllByTeacherId":
                try {
                    if(teacherId == -1) {
                        System.out.println(ERROR_PARAMETER+"--app.teacherId");
                        return;
                    }
                    System.out.println(INFO+asJsonString(subjectResource.readAllByTeacherId(teacherId)));
                } catch (HttpStatusCodeException e) {
                    System.out.println(NOT_FOUND);
                }
                break;

            case "update":
                try {
                    if(entity.equals("student")) {
                        if (studentId == -1 || firstName == null || lastName == null || gpa == -1) {
                            System.out.println(ERROR_PARAMETER + "--app.studentId, --app.firstName, --app.lastName and --app.gpa");
                            return;
                        }
                        studentResource.update(studentId, new StudentCreateDTO(firstName, lastName, gpa));
                    } else if(entity.equals("teacher")) {
                        if(teacherId == -1 || firstName == null || lastName == null) {
                            System.out.println(ERROR_PARAMETER+"--app.teacherId, --app.firstName and --app.lastName");
                            return;
                        }
                        teacherResource.update(teacherId, new TeacherCreateDTO(firstName,lastName));
                    } else {
                        if(name == null || lang == null || subjectId == -1 || teacherId == -1 || studentIds == null) {
                            System.out.println(ERROR_PARAMETER+"--app.name, --app.lang, --app.subjectId, --app.teacherId and --app.studentIds");
                            return;
                        }
                        subjectResource.update(subjectId,new SubjectCreateDTO(name,lang,teacherId,Arrays.stream(studentIds.split(",")).map(Integer::parseInt).collect(Collectors.toList())));
                    }
                } catch (HttpStatusCodeException e) {
                    System.out.println(NOT_FOUND);
                }
                break;

            case "deleteById":
                try {
                    if (entity.equals("student")) {
                        if (studentId == -1) {
                            System.out.println(ERROR_PARAMETER + "--app.studentId");
                            return;
                        }
                        studentResource.deleteById(studentId);
                    } else if(entity.equals("teacher")) {
                        if(teacherId == -1) {
                            System.out.println(ERROR_PARAMETER + "--app.teacherId");
                            return;
                        }
                        teacherResource.deleteById(teacherId);
                    } else {
                        if(subjectId == -1) {
                            System.out.println(ERROR_PARAMETER + "--app.subjectId");
                            return;
                        }
                        subjectResource.deleteById(subjectId);
                    }
                } catch (HttpStatusCodeException e) {
                    System.out.println(NOT_FOUND);
                }
                break;
            case "deleteWithNoSubject":
                try {
                    teacherResource.deleteWithNoSubject();
                } catch (HttpStatusCodeException e) {
                    System.out.println(NOT_FOUND);
                }
                break;
            default:
                System.out.println("[Error] This action "+action+" is not available for "+entity+" entity");
        }
    }

    /* Helper functions */
    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
