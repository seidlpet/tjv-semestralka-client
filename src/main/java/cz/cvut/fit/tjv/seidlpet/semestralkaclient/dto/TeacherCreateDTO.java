package cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto;

public class TeacherCreateDTO {
    private final String firstName;
    private final String lastName;

    public TeacherCreateDTO( String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
