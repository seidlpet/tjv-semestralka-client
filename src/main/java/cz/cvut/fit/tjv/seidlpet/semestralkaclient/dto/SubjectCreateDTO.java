package cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto;

import java.util.List;

public class SubjectCreateDTO {
    private final String name;
    private final String lang;
    private final int teacherId;
    private final List<Integer> students;

    public SubjectCreateDTO(String name, String lang, int teacherId, List<Integer> students) {
        this.name = name;
        this.lang = lang;
        this.teacherId = teacherId;
        this.students = students;
    }

    public String getName() {
        return name;
    }

    public String getLang() {
        return lang;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public List<Integer> getStudents() {
        return students;
    }
}
