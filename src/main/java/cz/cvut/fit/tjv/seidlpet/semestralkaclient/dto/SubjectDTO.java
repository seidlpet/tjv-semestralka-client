package cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto;

import java.util.List;

public class SubjectDTO {
    private final int id;
    private final String name;
    private final String lang;
    private final int teacherId;
    private final List<Integer> students;

    public SubjectDTO(int id, String name, String lang, int teacherId, List<Integer> students) {
        this.id = id;
        this.name = name;
        this.lang = lang;
        this.teacherId = teacherId;
        this.students = students;
    }

    public int getId() { return id; }

    public String getName() {
        return name;
    }

    public String getLang() {
        return lang;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public List<Integer> getStudents() {
        return students;
    }
}
