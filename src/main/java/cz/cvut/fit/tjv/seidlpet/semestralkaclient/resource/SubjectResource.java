package cz.cvut.fit.tjv.seidlpet.semestralkaclient.resource;

import cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto.SubjectCreateDTO;
import cz.cvut.fit.tjv.seidlpet.semestralkaclient.dto.SubjectDTO;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component
public class SubjectResource {
    private final RestTemplate restTemplate;

    public SubjectResource(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private static final String TEACHER_ID_LIST_URI = "/?teacherId={teacherId}";
    private static final String ROOT_RESOURCE_URL = "http://localhost:8080/api/v1/subject";
    private static final String ONE_URI = "/{id}";

    public URI create(SubjectCreateDTO data) {
        return restTemplate.postForLocation(ROOT_RESOURCE_URL,data);
    }

    public List<SubjectDTO> readAll() {
        return Arrays.asList(
                Objects.requireNonNull(
                    restTemplate.getForEntity(ROOT_RESOURCE_URL, SubjectDTO[].class).getBody()
                )
        );
    }

    public List<SubjectDTO> readAllByTeacherId(int id) {
        return Arrays.asList(
                Objects.requireNonNull(
                        restTemplate.getForEntity(ROOT_RESOURCE_URL+TEACHER_ID_LIST_URI,SubjectDTO[].class,id).getBody()
                )
        );
    }

    public SubjectDTO readById(int id) {
        return restTemplate.getForObject(ROOT_RESOURCE_URL+ONE_URI,SubjectDTO.class,id);
    }

    public void update(int id, SubjectCreateDTO data) {
        restTemplate.put(ROOT_RESOURCE_URL+ONE_URI,data,id);
    }

    public void deleteById(int id) {
        restTemplate.delete(ROOT_RESOURCE_URL+ONE_URI,id);
    }

}
